Xonotic Dæmon Glue
==================

This is an experimental glue for the [Dæmon Engine](https://github.com/DaemonEngine/Daemon) to run a minimal QuakeC VM for testing that DarkPlaces and Dæmon builtins work the same with. This effort is made in order to evaluate Dæmon as an engine that may be suitable to replace DarkPlaces.


Track work in progress
----------------------

See [xonotic#244](https://gitlab.com/xonotic/xonotic/issues/244) for the Dæmon port tracking issue.

See [daemon-glue#1](https://gitlab.com/xonotic/daemon-glue/issues/1) for the visual parity tracking issue.

See [Xonotic project](https://github.com/orgs/DaemonEngine/projects/7) on Dæmon upstream for efforts done to improve compatibility with Xonotic.

See [Standalone project](https://github.com/orgs/DaemonEngine/projects/9) on Dæmon upstream for efforts done to improve engine reusability.


Enter the Xonotic world
-----------------------

The following instructions assume this repository is cloned as a subdirectory of [Xonotic's repository](https://gitlab.com/xonotic/xonotic), with a file hierarchy looking like this:

```
xonotic/ ┬ all
         ├ d0_blind_id/
         ├ daemon-glue/
         ├ data/
         ├ netradiant/
         └ …
```

If you don't have the Xonotic repository yet, you can easily acquire it this way:

```sh
git clone 'https://gitlab.com/xonotic/xonotic.git'
cd 'xonotic'
```

You don't need DarkPlaces and some components, so you can optionally disable them:

```sh
touch 'darkplaces.no' 'data/xonotic-music.pk3dir.no'
```

Finally, run an update to fetch the missing subdirectories:

```sh
./all update
```


Clone this repository
---------------------

```sh
git clone --recurse-submodules 'https://gitlab.com/xonotic/daemon-glue.git'
cd 'daemon-glue'
```

If you forgot the `--recurse-submodules` option at clone time, you can fix it with this:

```sh
git submodule update --init --recursive
```


Build the engine and the glue
-----------------------------

Cog content generation tool is needed, it can be installed with this command, this has to be done only once:

```sh
pip install --user cogapp
```


By building gamecode as a native shared library with debug symbols, it allows easy debugging of both the engine and the glue:

```sh
cmake -S. -B'build' -G'Unix Makefiles' \
  -D'USE_BREAKPAD=OFF' -D'CMAKE_BUILD_TYPE=Debug' -D'USE_DEBUG_OPTIMIZE=OFF' \
  -D'BUILD_GAME_NACL=OFF' -D'BUILD_GAME_NACL_NEXE=OFF' -D'BUILD_GAME_NATIVE_EXE=OFF' \
  -D'BUILD_GAME_NATIVE_DLL=ON'
```

```sh
cmake --build 'build' -- -j$(nproc)
```


Build the game and a map
------------------------

Those commands assumes you are in the `daemon-glue` directory.

You have to build game code in order to run the game properly, a special branch has to be used, it may build dependencies like `d0_blind_id`, `ZIP=:` environment variable prevents the useless creation of a pk3 for the game code:

```sh
git -C ../data/xonotic-data.pk3dir checkout 'Mario/ecs_halfbaked'
ZIP=: ../all compile -qc
```

You may want to build at least one map, it may build dependencies like `q3map2`:

```sh
../all compile-map 'solarium'
```


Run the game and load a map
---------------------------

```sh
build/daemon \
  -set vm.cgame.type 3 -set vm.sgame.type 3 \
  -pakpath 'pkg' \
  -pakpath 'libs/daemon/pkg' \
  -pakpath '../data' \
  -set fs_basepak 'xonotic' \
  -set fs_legacypaks on \
  -set fs_maxSymlinkDepth 1 \
  -set r_dpMaterial on \
  -set r_dpBlend on \
  +devmap 'solarium'
```

You may want to run the game on `gdb` this way to debug it:

```sh
gdb -ex=run --args build/daemon \
  # usual options previously described
```


Some game commands and cvars to toy with
----------------------------------------

You may find this reminder useful:

```
cl_consoleCommand ""
listCvars *pattern*
listCmd pattern
grep word
r_customwidth 1280
r_customheight 720
vid_restart
r_allowResize 1
logs.logLevel.sgame.default "debug"
```