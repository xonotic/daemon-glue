#include "../Q1VM.hpp"

#include "com/Buffer.hpp"
#include <engine/client/cg_api.h>

#ifndef func_t
    #define func_t int
    #define string_t int
    #include <client/mprogdefs.hpp>
#else
    #undef func_t
    #undef string_t
#endif

#ifndef BUILTIN
#include "menu.hpp"

#undef BUILTIN
#define BUILTIN(vm, number, ret, id, params) \
    ret id params; auto _builtin_##id = Q1VM::Builtin<Q1VM::type::vm, number, Q1VM_MAKE_BUILTIN(id)>(); \
    ret id params
#endif

namespace ui {
    QStr strcat(Q1VM::Arguments args);
}

/*[[[cog
import cog_builtins
cog_builtins.parse('src/vm/sys/common.yml', 'ui')
]]]*/
namespace ui {
    using entity = edict_s&;
    using string = QStr;
    using vector = QVec3;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
#pragma GCC diagnostic ignored "-Wunused-parameter"
    BUILTIN(ui, 2, void, error, (string s)) {
        Sys::Error(s->c_str());
    }
    BUILTIN(ui, 4, void, print, (Q1VM::Arguments args)) {
        Log::Notice("%s", strcat(args)->c_str());
    }
    BUILTIN(ui, 13, void, localcmd, (string s)) {
        /* TODO */;
    }
    BUILTIN(ui, 14, float, cvar, (string s)) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 15, void, cvar_set, (string var, string val)) {
        /* TODO */;
    }
    BUILTIN(ui, 16, void, dprint, (string s)) {
        Log::Verbose("%s", s->c_str());
    }
    BUILTIN(ui, 17, string, ftos, (float f)) {
        return (static_cast<int>(f) == f) ? va("%.0f", f) : va("%f", f);
    }
    BUILTIN(ui, 18, float, fabs, (float f)) {
        return std::fabs(f);
    }
    BUILTIN(ui, 21, float, stof, (string s)) {
        try { return std::stof(s); } catch (std::invalid_argument &e) { return 0; };
    }
    BUILTIN(ui, 22, entity, spawn, ()) {
        return thisvm->spawn().data;
    }
    BUILTIN(ui, 34, float, rint, (float f)) {
        return std::rint(f);
    }
    BUILTIN(ui, 35, float, floor, (float f)) {
        return std::floor(f);
    }
    BUILTIN(ui, 36, float, ceil, (float f)) {
        return std::ceil(f);
    }
    BUILTIN(ui, 38, float, sin, (float f)) {
        return std::sin(f);
    }
    BUILTIN(ui, 39, float, cos, (float f)) {
        return std::cos(f);
    }
    BUILTIN(ui, 40, float, sqrt, (float f)) {
        return std::sqrt(f);
    }
    BUILTIN(ui, 45, float, bound, (float low, float x, float high)) {
        return std::min(std::max(low, x), high);
    }
    BUILTIN(ui, 46, float, pow, (float a, float b)) {
        return std::pow(a, b);
    }
    BUILTIN(ui, 47, void, copyentity, (entity from, entity to)) {
        thisvm->entityManager.copyEntity(from, to);
    }
    BUILTIN(ui, 48, float, fopen, (string filename, float mode)) {
         auto fd = fileHandle_t {}; trap_FS_FOpenFile(filename->c_str(), &fd, mode == 2 ? fsMode_t::FS_WRITE : mode == 1 ? fsMode_t::FS_APPEND : fsMode_t::FS_READ ); return fd ;
    }
    BUILTIN(ui, 50, string, fgets, (float fhandle)) {
         auto fd = fileHandle_t {static_cast<int>(fhandle)}; std::string ret; char c; for (;;) { trap_FS_Read(&c, 1, fd); if (c == 0 || c == '\n') break; ret += c; } return ret ;
    }
    BUILTIN(ui, 52, float, strlen, (string s)) {
        return s->length();
    }
    BUILTIN(ui, 53, string, strcat, (Q1VM::Arguments args)) {
        std::string ret; for (int i = 0; i < args.size(); ++i) ret += args.get<string>(i); return ret;
    }
    BUILTIN(ui, 54, string, substring, (string s, float start, float length)) {
        try { return s->substr(start, length); } catch(std::out_of_range e) { return ""; };
    }
    BUILTIN(ui, 56, string, strzone, (string s)) {
        return s;
    }
    BUILTIN(ui, 57, void, strunzone, (string s)) {
        ;
    }
    BUILTIN(ui, 60, float, isserver, ()) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 62, float, clientstate, ()) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 67, float, gettime, (float timer)) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 70, float, mod, (float val, float m)) {
        return static_cast<int32_t>(val) % static_cast<int32_t>(m);
    }
    BUILTIN(ui, 71, string, cvar_string, (string s)) {
         char buf[256]; trap_Cvar_VariableStringBuffer(s->c_str(), buf, sizeof(buf)); std::string ret(buf, sizeof(buf)); return ret ;
    }
    BUILTIN(ui, 74, float, search_begin, (string pattern, float caseinsensitive, float quiet)) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 76, float, search_getsize, (float handle)) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 79, float, num_for_edict, (entity ent)) {
        return static_cast<float>(EntityManager::identify(ent));
    }
    BUILTIN(ui, 80, entity, entitybyindex, (float num)) {
        return thisvm->entityManager.get(num).data;
    }
    BUILTIN(ui, 89, string, cvar_defstring, (string s)) {
         char buf[256]; trap_Cvar_VariableStringBuffer(s->c_str(), buf, sizeof(buf)); ;
    }
    BUILTIN(ui, 221, float, strstrofs, (string str, string sub, float startpos)) {
         const auto pos = str->find(sub, startpos); if (pos == std::string::npos) return -1; return pos;
    }
    BUILTIN(ui, 226, string, infoadd, (string info, string key, string value)) {
        return ""/* TODO */;
    }
    BUILTIN(ui, 227, string, infoget, (string info, string key)) {
        return ""/* TODO */;
    }
    BUILTIN(ui, 228, float, strncmp, (string s1, string s2, float len)) {
        return s1->compare(s2) == 0/* TODO */;
    }
    BUILTIN(ui, 349, float, isdemo, ()) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 440, float, buf_create, ()) {
        return Buffer::New();
    }
    BUILTIN(ui, 441, void, buf_del, (float bufhandle)) {
        Buffer::Delete(static_cast<int>(bufhandle));
    }
    BUILTIN(ui, 446, string, bufstr_get, (float bufhandle, float string_index)) {
        return "TODO"/* TODO */;
    }
    BUILTIN(ui, 447, void, bufstr_set, (float bufhandle, float string_index, string str)) {
        /* TODO */;
    }
    BUILTIN(ui, 471, float, asin, (float f)) {
        return std::asin(f);
    }
    BUILTIN(ui, 472, float, acos, (float f)) {
        return std::acos(f);
    }
    BUILTIN(ui, 473, float, atan, (float f)) {
        return std::atan(f);
    }
    BUILTIN(ui, 474, float, atan2, (float a, float b)) {
        return std::atan2(a, b);
    }
    BUILTIN(ui, 475, float, tan, (float f)) {
        return std::tan(f);
    }
    BUILTIN(ui, 484, string, strreplace, (string search, string replace, string subject)) {
        return subject/* TODO */;
    }
    BUILTIN(ui, 494, float, crc16, (float caseinsensitive, string s)) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 510, string, uri_escape, (string in)) {
        return in/* TODO */;
    }
    BUILTIN(ui, 511, string, uri_unescape, (string in)) {
        return in/* TODO */;
    }
    BUILTIN(ui, 601, void, setkeydest, (float dest)) {
        /* TODO */;
    }
    BUILTIN(ui, 602, float, getkeydest, ()) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 603, void, setmousetarget, (float trg)) {
        /* TODO */;
    }
    BUILTIN(ui, 604, float, getmousetarget, ()) {
        return 0/* TODO */;
    }
    BUILTIN(ui, 626, string, getgamedirinfo, (float n, float prop)) {
        return "" /* TODO */;
    }
    BUILTIN(ui, 627, string, sprintf, (string format, Q1VM::Arguments args)) {
         std::string ret; int i = 0; for (const char *s = format->c_str(); *s; ++s) { if (*s != '%') { ret += *s; } else { s += 1; if (*s == '%') { ret += '%'; } else if (*s == 's') { ret += args.get<string>(i++); } else if (*s == 'f') { ret += std::to_string(args.get<float>(i++)); } else {
} } } return ret;
    }
    BUILTIN(ui, 639, string, digest_hex, (string digest, string data)) {
        return data/* TODO */;
    }
#pragma GCC diagnostic pop
}
//[[[end]]] (checksum: 816a3b18564f5bdc869e7bd48ffee799)
