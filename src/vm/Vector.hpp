#pragma once

#include <common/math/Vector.h>
#include <common/Common.h>

struct QVec3 {
    float x, y, z;

    QVec3(float x, float y, float z) : x{x}, y{y}, z{z}
    { }

    QVec3(float *ptr) : QVec3{ptr[0], ptr[1], ptr[2]}
    { }

    QVec3(Math::Vec3 ptr) : QVec3{ptr[0], ptr[1], ptr[2]}
    { }

    operator Math::Vec3() const
    { return Math::Vec3(x, y, z); }

    void Store(float *ptr) const
    { Math::Vec3(*this).Store(ptr); }

    QVec3 Normalized() const
    {
        vec3_t v;
        Store(v);
        VectorNormalize(v);
        return v;
    }

    float Length() const
    {
        vec3_t v;
        Store(v);
        return DotProduct(v, v);
    }

    QVec3 operator=(QVec3 o)
    {
        this->x = o.x;
        this->y = o.y;
        this->z = o.z;
        return *this;
    }

};
