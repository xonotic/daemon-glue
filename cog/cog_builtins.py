import cog
import re
import yaml

def parse(path, vm):
    def _parse(k, v):
        (_, body) = v
        number = _.get(vm, None)
        if not number:
            return None
        (ret, _) = k.split(' ', 1)
        (id, params) = _.split('(', 1)
        params = params.replace('...', 'Q1VM::Arguments ')
        params = '(' + re.sub(r'\.([^ ]+)', r'\1 edict_s::*', params)
        return (vm, number, ret, id, params, body)
    defs = yaml.load(open(path))
    defs = [_parse(k, defs[k]) for k in defs]
    defs = [it for it in defs if it is not None]
    defs.sort(key=lambda it: (it[1], it[3]))
    cog.outl("namespace {} {{".format(vm))
    cog.outl("    using entity = edict_s&;")
    cog.outl("    using string = QStr;")
    cog.outl("    using vector = QVec3;")
    cog.outl("""\
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
#pragma GCC diagnostic ignored "-Wunused-parameter"\
""")
    for it in defs:
        cog.outl("    BUILTIN({}, {}, {}, {}, {}) {{\n        {};\n    }}".format(*it))
    cog.outl("#pragma GCC diagnostic pop")
    cog.outl("}")
