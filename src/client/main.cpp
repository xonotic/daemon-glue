#include <engine/client/cg_api.h>
#include <engine/client/cg_msgdef.h>

#include <shared/CommonProxies.h>
#include <shared/VMMain.h>
#include <shared/client/cg_api.h>

#include <vm/Q1VM.hpp>
#include <vm/sys/client.hpp>
#include "commands.hpp"
#include "cvars.hpp"

static Q1VM *q1vm;
static Q1VM *mvm;

void VM::VMInit()
{
}

static struct {
    int vidWidth, vidHeight;
    qhandle_t chars;
} d2d;

float cmdscale(usercmd_t &cmd, bool zFlight = false)
{
    using namespace std;
    const auto f = static_cast<int8_t>(abs(cmd.forwardmove));
    const auto r = static_cast<int8_t>(abs(cmd.rightmove));
    auto lc = max(f, r);
    auto m2 = cmd.forwardmove * cmd.forwardmove
              + cmd.rightmove * cmd.rightmove;
    if (zFlight) {
        const auto u = static_cast<int8_t>(abs(cmd.upmove));
        lc = max(lc, u);
        m2 += cmd.upmove * cmd.upmove;
    }
    if (!lc) return 0;
    const auto m = sqrt(m2);
    return static_cast<float>(320 * lc / (127 * m));
}

void SCR_DrawSmallUnichar(int x, int y, int ch)
{
    if (ch >= 0x100) return;
    if (ch == ' ') return;
    if (y < -SMALLCHAR_HEIGHT) return;

    const auto row = (ch >> 4) & 0x0F;
    const auto col = (ch >> 0) & 0x0F;
    const auto size = 0.0625f;

    // adjust for baseline
    const auto CONSOLE_FONT_VPADDING = 0.3;
    trap_R_DrawStretchPic(x, y - static_cast<int>(SMALLCHAR_HEIGHT / (CONSOLE_FONT_VPADDING + 1)),
                          SMALLCHAR_WIDTH, SMALLCHAR_HEIGHT,
                          col * size, row * size,
                          col * size + size, row * size + size,
                          d2d.chars);
}

void VM::VMHandleSyscall(uint32_t id, Util::Reader reader)
{
    const auto major = id >> 16;
    const auto minor = static_cast<cgameExport_t>(id & 0xFFFF);
    if (major == VM::QVM) {
        switch (minor) {
            default:
                Sys::Error("VM::VMHandleSyscall: unknown cgame command %i", minor);
            case CG_STATIC_INIT: {
                auto func = [](int milliseconds) {
                    VM::InitializeProxies(milliseconds);
                    FS::Initialize();
                    srand((unsigned int) time(nullptr));
                    cmdBuffer.Init();
                };
                IPC::HandleMsg<CGameStaticInitMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_INIT: {
                auto func = [](int serverMessageNum, int clientNum, glconfig_t gl, GameStateCSs gamestate) {
                    Q_UNUSED(serverMessageNum);
                    Q_UNUSED(clientNum);
                    Cvars::init();
                    Commands::init();
                    mvm = Q1VM::load(Q1VM::type::ui, "menu.dat");
                    mvm->exec("m_init");
                    q1vm = Q1VM::load(Q1VM::type::cl, "csprogs.dat");
                    q1vm->exec(&globalvars_t::CSQC_Init);
                    trap_Key_SetCatcher(0);
                    trap_SetUserCmdValue(0, 0, 1); // first two are like qc 'impulse', last is sensitivity
                    d2d.vidWidth = gl.vidWidth;
                    d2d.vidHeight = gl.vidHeight;
                    d2d.chars = trap_R_RegisterShader("gfx/2d/bigchars", RSF_DEFAULT);
                    {
                        auto info = gamestate[CS_SERVERINFO];
                        auto mapname = Info_ValueForKey(info.c_str(), "mapname");
                        trap_CM_LoadMap(mapname);
                        trap_R_LoadWorldMap(va("maps/%s.bsp", mapname));
                        // register the inline models
                        int numInlineModels = trap_CM_NumInlineModels();
                        if (numInlineModels > MAX_SUBMODELS) {
                            Sys::Error("MAX_SUBMODELS (%d) exceeded by %d", MAX_SUBMODELS,
                                       numInlineModels - MAX_SUBMODELS);
                        }
                        for (int i = 1; i < numInlineModels; i++) {
                            trap_R_RegisterModel(va("*%i", i));
                        }
                    }
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameInitMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_SHUTDOWN: {
                auto func = [] { };
                IPC::HandleMsg<CGameShutdownMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_DRAW_ACTIVE_FRAME: {
                auto func = [](int serverTime, bool demoPlayback) {
                    Q_UNUSED(serverTime);
                    Q_UNUSED(demoPlayback);
                    Cvars::update();
                    static int ms_old;
                    int ms = trap_Milliseconds();
                    int frameTime = ms - ms_old;
                    ms_old = ms;

                    float fps;
                    {
                        const int FPS_FRAMES = 32;
                        static struct {
                            int buf[FPS_FRAMES];
                            int index;
                            float fps;
                        } s = {};
                        s.buf[s.index++ % FPS_FRAMES] = frameTime;
                        if (s.index > FPS_FRAMES) {
                            int total = 0;
                            for (int i = 0; i < FPS_FRAMES; i++) {
                                total += s.buf[i];
                            }
                            s.fps = 1000.0f * FPS_FRAMES / total;
                        }
                        fps = s.fps;
                    }

                    q1vm->globalVars.time = ms / 1000.0f;
                    q1vm->globalVars.frametime = frameTime / 1000.0f;

                    usercmd_t cmd;
                    trap_GetUserCmd(trap_GetCurrentCmdNumber(), &cmd);
                    float f = cmdscale(cmd);
                    q1vm->globalVars.input_movevalues = {cmd.forwardmove * f, cmd.rightmove * f, cmd.upmove * f};
                    auto dmouse = QVec3{float(SHORT2ANGLE(cmd.angles[PITCH])), float(SHORT2ANGLE(cmd.angles[YAW])),
                                        float(SHORT2ANGLE(cmd.angles[ROLL]))};
                    q1vm->globalVars.input_angles = dmouse;

                    q1vm->globalVars.input_buttons = cmd.upmove > 0 ? BIT(1) : 0;
                    q1vm->exec(&globalvars_t::CSQC_UpdateView);
                    {
                        trap_R_ClearScene();
                        auto refdef = refdef_t {};
                        refdef.width = d2d.vidWidth;
                        refdef.height = d2d.vidHeight;
                        refdef.fov_y = 90;
                        refdef.fov_x = float(2 * RAD2DEG(atan2(refdef.width, refdef.height / tan(DEG2RAD(refdef.fov_y) / 2))));
                        refdef.gradingWeights[0] = 0;
                        refdef.gradingWeights[1] = 0.5;
                        refdef.gradingWeights[2] = 0.5;
                        refdef.gradingWeights[3] = 0.5;
                        View::origin.Store(refdef.vieworg);
                        vec3_t angles;
                        q1vm->globalVars.input_angles.Store(angles);
                        AnglesToAxis(angles, refdef.viewaxis);
                        static bool loadedWorld = false;
                        if (loadedWorld) // wait for trap_R_LoadWorldMap
                            trap_R_RenderScene(&refdef);
                        loadedWorld = true;

                        trap_R_SetColor(Color::Blue);
                        int x = 0;
                        for (const char *s = va("%.0f", fps); *s; ++s) {
                            SCR_DrawSmallUnichar(x += SMALLCHAR_WIDTH, 10, *s);
                        }
                    }
                    mvm->set(OFS_PARAM(0), d2d.vidWidth);
                    mvm->set(OFS_PARAM(1), d2d.vidHeight);
                    mvm->exec("m_draw");
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameDrawActiveFrameMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_CROSSHAIR_PLAYER: {
                auto func = [](int &player) {
                    Q_UNUSED(player);
                };
                IPC::HandleMsg<CGameCrosshairPlayerMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_KEY_EVENT: {
                auto func = [](Keyboard::Key key, bool down) {
                    Q_UNUSED(key);
                    Q_UNUSED(down);
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameKeyEventMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_MOUSE_EVENT: {
                auto func = [](int dx, int dy) {
                    Q_UNUSED(dx);
                    Q_UNUSED(dy);
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameMouseEventMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_MOUSE_POS_EVENT: {
                auto func = [](int x, int y) {
                    Q_UNUSED(x);
                    Q_UNUSED(y);
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameMousePosEventMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_CHARACTER_INPUT_EVENT: {
                auto func = [](char c) {
                    Q_UNUSED(c);
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameCharacterInputMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_ROCKET_VM_INIT: {
                auto func = [](glconfig_t gl) {
                    Q_UNUSED(gl);
                };
                IPC::HandleMsg<CGameRocketInitMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_ROCKET_FRAME: {
                auto func = [](cgClientState_t cs) {
                    Q_UNUSED(cs);
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameRocketFrameMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_CONSOLE_LINE: {
                auto func = [](std::string str) {
                    Q_UNUSED(str);
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameConsoleLineMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case CG_FOCUS_EVENT: {
                auto func = [](bool focus) {
                    trap_SetMouseMode(focus ? MouseMode::Deltas : MouseMode::SystemCursor);
                    cmdBuffer.TryFlush();
                };
                IPC::HandleMsg<CGameFocusEventMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
        }
    } else if (major < VM::LAST_COMMON_SYSCALL) {
        VM::HandleCommonSyscall(major, minor, std::move(reader), VM::rootChannel);
    } else {
        Sys::Error("VM::VMHandleSyscall: unhandled VM major syscall number %i", major);
    }
}
