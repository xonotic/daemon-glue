#pragma once

#include <cstddef>
#include <vector>
#include "Entity.hpp"
#include "String.hpp"

class Q1VM;

class EntityManager {
public:
    EntityManager(Q1VM &vm, size_t entsize);

    static uint32_t identify(edict_s &data);

    static void remove(edict_s &data);

    Entity spawn();

    Entity get(uint32_t ent) const;

    size_t address(uint32_t ent, int fld) const;

    template<class T>
    T read(uint32_t ent, int fld) const;

    template<class T>
    void write(size_t address, T value);

    edict_s &next(edict_s &e);

    edict_s &find(edict_s &start, float edict_s::* fld, float match);
    edict_s &find(edict_s &start, int edict_s::* fld, QStr match);

    edict_s &findChain(float edict_s::* fld, float match, int edict_s::* chain);
    edict_s &findChain(QStr edict_s::* fld, QStr match, int edict_s::* chain);

    void copyEntity(edict_s &from, edict_s &to);

private:
    Q1VM &vm;
    uint32_t autoid = 0;
    size_t entsize;
    std::vector<Entity *> entities;

    uint32_t *deref(size_t address) const;
};

template<class T>
T EntityManager::read(uint32_t ent, int fld) const
{
    return *reinterpret_cast<T *>(deref(address(ent, fld)));
}

template<class T>
void EntityManager::write(size_t address, T value)
{
    *reinterpret_cast<T *>(deref(address)) = value;
}
