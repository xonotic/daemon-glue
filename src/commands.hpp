#pragma once

#include <engine/client/cg_api.h>
#include <engine/qcommon/q_shared.h>

#define COMMAND(id)                             \
    void id();                                  \
    Commands::Command Command_##id(#id, &id);   \
    void id()

class Commands {

    typedef void (*func)();

    Commands()
    { }

    std::map<std::string, func> funcs;

public:
    static Commands &instance()
    {
        static Commands it;
        return it;
    }

    static void init()
    {
        for (auto &e : instance().funcs) {
            trap_AddCommand(e.first.c_str());
        }
    }

    static void execute(char const *name)
    {
        auto &commands = instance().funcs;
        if (commands.count(name)) {
            commands[name]();
        }
    }

    struct Command {
        Command(char const *name, func f)
        {
            Commands::instance().add(name, f);
        }
    };

    void add(char const *name, func f)
    {
        funcs[name] = f;
    }

};
