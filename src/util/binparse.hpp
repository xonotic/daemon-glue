#pragma once

#include <vector>

namespace binparse {
    template<class T>
    T read(uint8_t *data)
    {
        return T::read(data);
    }

    template<class T>
    void readSeek(T &t, uint8_t *&data)
    {
        t = read<T>(data);
        data += sizeof(T);
    }

    template<class T>
    void readList(std::vector<T> &it, uint8_t *data, size_t count)
    {
        for (auto i = 0u; i < count; i++) {
            it.push_back(read<T>(data));
            data += sizeof(T);
        }
    }

    template<>
    int32_t read(uint8_t *data)
    {
        return data[0] | data[1] << 8 | data[2] << 16 | data[3] << 24;
    }

    template<>
    uint32_t read(uint8_t *data)
    {
        return data[0] | data[1] << 8 | data[2] << 16 | data[3] << 24;
    }

    template<>
    uint16_t read(uint8_t *data)
    {
        return data[0] | data[1] << 8;
    }
}
