#pragma once

#define BUILTIN(vm, number, ret, id, params) \
    ret id params; inline ret _func_unused_##number params

#include "client.cpp"
